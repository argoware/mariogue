<!--
SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Mariogue

A side-scrolling, roguelike, platform, terminal game.

## Instructions

The aim of the game is to achieve a high score.  
You score points going to new rooms, killing enemies (shown as red `O`s) and getting bonuses (shown as magenta `?`s).  
Avoid damage pickups (shown as red `?`s) and enemy attacks.  
You can regain life with health pickups (shown as green `?`s).

- WASD/Arrow keys: move
- Space/J: attack
- X: quit

## Compile and run

Install g++, cmake, ncurses, clang-tidy, clang-format.

```
./build.sh
./Mariogue
```

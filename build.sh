# SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi
#
# SPDX-License-Identifier: GPL-3.0-or-later

clang-format -i ./include/*
clang-format -i ./src/*
rm -r build-dir
mkdir build-dir
cd build-dir
cmake .. && make
cp Mariogue ..

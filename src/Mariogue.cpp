// SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "Player.h"

#include <chrono>
#include <iostream>
#include <ncurses.h>

int main() {
	std::cout << "Welcome to Mariogue!\n";
	std::cout << "Press x to quit.\n";
	long seed;
	do {
		std::cout << "Seed (-1 for random): ";
		std::cin >> seed;
	} while (std::cin.fail() || (seed < -1));
	if (seed == -1) {
		seed = std::chrono::system_clock::now().time_since_epoch().count();
		std::cout << "Seed: " << seed << std::endl;
	}

	initscr();
	noecho();
	cbreak();
	curs_set(FALSE);
	start_color();
	timeout(0);
	init_pair(1, COLOR_BLACK, COLOR_GREEN); // Life+
	init_pair(2, COLOR_BLACK, COLOR_RED); // Life-
	init_pair(20, COLOR_CYAN, COLOR_BLACK); // Player
	init_pair(21, COLOR_BLACK, COLOR_WHITE); // Wall
	init_pair(22, COLOR_YELLOW, COLOR_BLACK); // Ladder
	init_pair(23, COLOR_BLACK, COLOR_YELLOW); // Door
	init_pair(11, COLOR_GREEN, COLOR_BLACK); // Bonus_life
	init_pair(12, COLOR_MAGENTA, COLOR_BLACK); // Bonus_score
	init_pair(13, COLOR_RED, COLOR_BLACK); // Bonus_damage, enemy
	init_pair(30, COLOR_CYAN, COLOR_RED); // Damaged player
	init_pair(31, COLOR_RED, COLOR_YELLOW); // Damaged enemy

	int ymax;
	int xmax;
	getmaxyx(stdscr, ymax, xmax);

	WINDOW* playwin = newwin(20, 50, (ymax / 2) - 10, (xmax / 2) - 25);
	auto* p = new Player(playwin, seed);
	int mv = 0;

	do {
		p->updatePlayer();
		p->printRoom();
		p->display(xmax);
		mv = p->getMv();
	} while (mv != 'x' && mv != 'X' && !p->isDead());
	endwin();

	std::cout << "Final score: " << p->getScore() << std::endl;

	return 0;
}

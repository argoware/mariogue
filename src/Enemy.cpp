// SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "Enemy.h"

Enemy::Enemy() = default;

Enemy::Enemy(int y, int x, int life, int motionRange, int attackRange, int attackValue) {
	this->y = y;
	this->x = x;
	fixed_x = x;
	lifePoints = life;
	hit = false;
	if (motionRange <= 0) {
		motionRange = 1;
	}
	this->motionRange = motionRange;
	if (attackRange <= 0) {
		attackRange = 1;
	}
	this->attackRange = attackRange;
	if (attackValue <= 0) {
		attackValue = 1;
	}
	this->attackValue = attackValue;
}

// Decrease life by 'n' points
void Enemy::decrLife(int n) {
	lifePoints -= n;
}

int Enemy::getAttackRange() const {
	return attackRange;
}

int Enemy::getAttackValue() const {
	return attackValue;
}

// Return x-coord of centre of movement
int Enemy::getFixedX() const {
	return fixed_x;
}

bool Enemy::getHit() const {
	return hit;
}

int Enemy::getLife() const {
	return lifePoints;
}

int Enemy::getMotionRange() const {
	return motionRange;
}

// Return X-coordinate
int Enemy::getX() const {
	return x;
}

// Return Y-coordinate
int Enemy::getY() const {
	return y;
}

// Moves horizontally by 'deltaX', left if neg, right if pos
void Enemy::move(int deltaX) {
	x += deltaX;
}

void Enemy::setHit(bool a) {
	hit = a;
}

// SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "Bonus.h"

Bonus::Bonus() = default;

Bonus::Bonus(int y, int x, Effect effect, int value) {
	assert(x >= 0 && y >= 0);
	this->x = x;
	this->y = y;
	this->effect = effect;
	this->value = value;
}

Effect Bonus::getEffect() const {
	return effect;
}

int Bonus::getValue() const {
	return value;
}

int Bonus::getX() const {
	return x;
}

int Bonus::getY() const {
	return y;
}

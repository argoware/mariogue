// SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "Player.h"

Player::Player(WINDOW* win, long seed) {
	Room room {seed, 20, 50, 0};
	this->seed = seed;
	room.set(room.getEntranceY(), 0, Tile::Wall);
	rooms.push_back(room);
	roomIndex = 0;
	lifePoints = 100;
	hit = false;
	yLoc = room.getEntranceY();
	xLoc = 1;
	character = '@';
	this->win = win;
	score = 0;
	attackRange = 4;
	attackValue = 6;
	keypad(win, true);
}

// Area attack on all enemies in range
void Player::attack() {
	int tmp = rooms[roomIndex].attackEnemies(yLoc, xLoc, attackRange, attackValue);
	score += tmp;
	changeLife(tmp / 2);
}

// Receive damage from all enemies in range
void Player::bleed() {
	int damages = rooms[roomIndex].attackPlayer(yLoc, xLoc);
	if (damages > 0) {
		lifePoints -= damages;
		hit = true;
	}
}

// Increase or decrease AttackRange by 'n' points
void Player::changeAttackRange(int n) {
	attackRange += n;
}

// Increase or decrease attackValue by 'n' points
void Player::changeAttackValue(int n) {
	attackValue += n;
}

// Increase or decrease life by 'n' points
void Player::changeLife(int n) {
	lifePoints = std::min(lifePoints + n, 100);
}

// Returns pointer to current Room
Room* Player::curRoom() {
	return &rooms[roomIndex];
}

// Display Player on map
void Player::display(int xmax) {
	clear();
	for (int i = 0; (i < 100); i += 10) {
		if (i < lifePoints) {
			attron(COLOR_PAIR(1));
			mvaddch(0, (xmax / 2) - 25 + i / 10, ' ');
			attroff(COLOR_PAIR(1));
		} else {
			attron(COLOR_PAIR(2));
			mvaddch(0, (xmax / 2) - 25 + i / 10, ' ');
			attroff(COLOR_PAIR(2));
		}
	}
	mvprintw(1, (xmax / 2) - 25, "Score: %d", score);

	mvprintw(1, (xmax / 2) + 16, "Room: %3d", roomIndex + 1);
	refresh();

	bleed();
	wattron(win, COLOR_PAIR(hit ? 30 : 20));
	mvwaddch(win, yLoc, xLoc, character);
	wattroff(win, COLOR_PAIR(hit ? 30 : 20));
	wrefresh(win);
	hit = false;
}

// Returns index of current Room
int Player::getIndex() const {
	return roomIndex;
}

// Selects movement direction/attack enemies
int Player::getMv() {
	int choice = wgetch(win);
	switch (choice) {
		case KEY_UP:
		case 'w':
		case 'W':
			move(0, 1);
			break;
		case KEY_DOWN:
		case 's':
		case 'S':
			move(0, -1);
			break;
		case KEY_LEFT:
		case 'a':
		case 'A':
			move(-1, 0);
			break;
		case KEY_RIGHT:
		case 'd':
		case 'D':
			move(1, 0);
			break;
		case ' ':
		case 'j':
		case 'J':
			attack();
			break;
		default:
			break;
	}
	wrefresh(win);
	return (choice);
}

// Return current score
int Player::getScore() const {
	return score;
}

// Increase score by 'n' points
void Player::incrScore(int n) {
	score += n;
}

// Returns life status of Player
bool Player::isDead() {
	if (lifePoints > 0) {
		return false;
	} else {
		clear();
		refresh();
		wclear(win);
		int ymax;
		int xmax;
		getmaxyx(stdscr, ymax, xmax);
		attron(COLOR_PAIR(13));
		mvprintw((ymax / 2) - 2, (xmax / 2) - 5, "GAME OVER!");
		attroff(COLOR_PAIR(13));
		attron(COLOR_PAIR(11));
		mvprintw((ymax / 2), (xmax / 2) - 9, "Final score: %5d", score);
		attroff(COLOR_PAIR(11));
		mvprintw((ymax / 2) + 2, (xmax / 2) - 8, "Press x to quit.");
		refresh();
		int mv = 0;
		do {
			mv = getch();
		} while (mv != 'x' && mv != 'X');
		return true;
	}
}

// Implements movements in each direction
void Player::move(int deltaX, int deltaY) {
	Room curr = rooms[roomIndex];

	if (deltaY == 0) { // Move right/left
		if (curr(yLoc + 1, xLoc + deltaX) == Tile::Wall || curr(yLoc + 1, xLoc + deltaX) == Tile::Ladder) {
			if (curr(yLoc, xLoc + deltaX) == Tile::Door) {
				if (deltaX == -1) {
					prevRoom();
				} else {
					nextRoom();
				}
			} else if (curr(yLoc, xLoc + deltaX) != Tile::Wall) {
				xLoc += deltaX;
			}
		}
	} else if (deltaY == 1) { // Move up
		if (curr(yLoc, xLoc) == Tile::Ladder) {
			yLoc--;
		}
	} else if (deltaY == -1) { // Move down
		if (curr(yLoc + 1, xLoc) == Tile::Ladder) {
			yLoc++;
		}
	}
}

// Move to next room. If not present, generate next Room.
// Place Player next to Entrance of new Room.
void Player::nextRoom() {
	if (roomIndex == rooms.size() - 1) {
		Room room {++seed, 20, 50, roomIndex + 1};
		rooms.push_back(room);
		attackValue += 2;
		attackRange = std::min(attackRange + roomIndex / 2, 8);
	}
	roomIndex++;
	yLoc = rooms[roomIndex].getEntranceY();
	xLoc = 1;
}

// Moves to previous room. Place Player next to Exit of previous Room.
void Player::prevRoom() {
	if (roomIndex > 0) {
		roomIndex--;
		xLoc = rooms[roomIndex].getWidth() - 2;
		yLoc = rooms[roomIndex].getExitY();
	}
}

// Prints current Room
void Player::printRoom() {
	Room room = rooms[roomIndex];
	for (int y = 0; y < room.getHeight(); ++y) {
		for (int x = 0; x < room.getWidth(); ++x) {
			switch (room(y, x)) {
				case Tile::Empty:
					mvwaddch(win, y, x, ' ');
					break;
				case Tile::Wall:
					wattron(win, COLOR_PAIR(21));
					mvwaddch(win, y, x, ' ');
					wattroff(win, COLOR_PAIR(21));
					break;
				case Tile::Door:
					wattron(win, COLOR_PAIR(23));
					mvwaddch(win, y, x, ' ');
					wattroff(win, COLOR_PAIR(23));
					break;
				case Tile::Ladder:
					wattron(win, COLOR_PAIR(22));
					mvwaddch(win, y, x, '#');
					wattroff(win, COLOR_PAIR(22));
					break;
				case Tile::Bonus:
					if (room.getBonus(y, x)->getEffect() == Effect::Life) {
						wattron(win, COLOR_PAIR(11));
						mvwaddch(win, y, x, '?');
						wattroff(win, COLOR_PAIR(11));
					} else if (room.getBonus(y, x)->getEffect() == Effect::Score) {
						wattron(win, COLOR_PAIR(12));
						mvwaddch(win, y, x, '?');
						wattroff(win, COLOR_PAIR(12));
					} else {
						wattron(win, COLOR_PAIR(13));
						mvwaddch(win, y, x, '?');
						wattroff(win, COLOR_PAIR(13));
					}
					break;
				case Tile::Enemy:
					if (room.getEnemy(y, x)->getHit()) {
						wattron(win, COLOR_PAIR(31));
						mvwaddch(win, y, x, 'O');
						wattroff(win, COLOR_PAIR(31));
						room.getEnemy(y, x)->setHit(false);
					} else {
						wattron(win, COLOR_PAIR(13));
						mvwaddch(win, y, x, 'O');
						wattroff(win, COLOR_PAIR(13));
					}
					break;
			}
		}
	}
}

// Pick up bonuses. Update bonus list. Moves enemies.
void Player::updatePlayer() {
	rooms[roomIndex].update();
	Bonus* b = rooms[roomIndex].getBonus(yLoc, xLoc);
	if (b == nullptr) {
		return;
	} else if (b->getEffect() == Effect::Life) {
		if (lifePoints + b->getValue() <= 100) {
			changeLife(b->getValue());
			rooms[roomIndex].removeBonus(yLoc, xLoc);
		}
	} else if (b->getEffect() == Effect::Score) {
		incrScore(b->getValue());
		rooms[roomIndex].removeBonus(yLoc, xLoc);
	} else if (b->getEffect() == Effect::Damage) {
		changeLife(-b->getValue());
		hit = true;
		rooms[roomIndex].removeBonus(yLoc, xLoc);
	}
}

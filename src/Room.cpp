// SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "Room.h"

int Room::randomInt(int min, int max) {
	std::uniform_int_distribution<> random(min, max);
	return random(gen);
}

void Room::set(int y, int x, Tile tile) {
	assert(x >= 0 && x < w && y >= 0 && y < h);
	room[y * w + x] = tile;
}
Room::Room() = default;

Room::Room(long int seed, int height, int width, int diff) {
	gen.seed(seed);
	// Generate height if not defined
	if (height == 0) {
		h = randomInt(10, 100);
	} else {
		h = height;
	}
	// Generate width if not defined
	if (width == 0) {
		w = randomInt(10, 100);
	} else {
		w = width;
	}
	// Minimum height: 6 (h/3 >= 2)
	// Minimum width: 5
	assert(h >= 6 && w >= 5);
	room = Vector<Tile>(w * h, Tile::Empty);
	// Generate horizontal walls
	for (int i = 0; i < w; ++i) {
		set(0, i, Tile::Wall);
		set(h - 1, i, Tile::Wall);
	}
	// Generate vertical walls
	for (int i = 1; i < h - 1; ++i) {
		set(i, 0, Tile::Wall);
		set(i, w - 1, Tile::Wall);
	}
	// Generate floors
	int floor_height = randomInt(2, h / 3);
	for (int y = h - 1 - floor_height; y >= 2; y -= floor_height) {
		// Platforms minimum length: 3
		int start = randomInt(1, w - 4);
		int end = randomInt(start + 2, w - 2);
		int ladder = randomInt(start, end);
		for (int x = start; x <= end; ++x) {
			if (x == ladder) {
				set(y, x, Tile::Ladder);
				for (int tempY = y + 1; (*this)(tempY, x) != Tile::Wall; ++tempY) {
					set(tempY, x, Tile::Ladder);
				}
			} else {
				set(y, x, Tile::Wall);
			}
		}
	}
	int floorNumber = (h - 3) / floor_height;
	// Generate entrance
	entrance = h - 2 - randomInt(0, floorNumber) * floor_height;
	set(entrance, 0, Tile::Door);
	// Create a platform to connect the entrance with the existing floor
	for (int x = 1; (*this)(entrance + 1, x) != Tile::Wall; ++x) {
		if ((*this)(entrance + 1, x) == Tile::Empty) {
			set(entrance + 1, x, Tile::Wall);
		}
	}
	// Generate exit
	exit = h - 2 - randomInt(0, floorNumber) * floor_height;
	set(exit, w - 1, Tile::Door);
	// Create a platform to connect the exit with the existing floor
	for (int x = w - 2; (*this)(exit + 1, x) != Tile::Wall; --x) {
		// Keep the ladders
		if ((*this)(exit + 1, x) == Tile::Empty) {
			set(exit + 1, x, Tile::Wall);
		}
	}
	// Generate bonuses
	for (int i = randomInt(w * h / 100, w * h / 80); i > 0; --i) {
		int randomX = randomInt(1, w - 2);
		int randomY = randomInt(1, h - 2);
		while ((*this)(randomY + 1, randomX) != Tile::Wall) {
			++randomY;
		}
		if (getBonus(randomY, randomX) == nullptr && (*this)(randomY, randomX) != Tile::Ladder) {
			Bonus b {randomY, randomX, static_cast<Effect>(randomInt(0, int(Effect::Damage))), randomInt(diff, diff + 10)};
			bonuses.push_back(b);
		}
	}
	// Generate Enemies
	for (int i = randomInt(w * h / 100, w * h / 80); i > 0; --i) {
		int diffVal = randomInt((diff + 1) * 10 - 5, (diff + 1) * 10 + 5); //each monster in room has different random stats
		int randomX = randomInt(1, w - 2);
		int randomY = randomInt(1, h - 2);
		while ((*this)(randomY + 1, randomX) != Tile::Wall) {
			++randomY;
		}
		if (getEnemy(randomY, randomX) == nullptr && (*this)(randomY, randomX) != Tile::Ladder) {
			int life = (int)(diffVal * 0.4);
			int motionRange = (int)(diffVal * 0.2);
			int attackRange = (int)(diffVal * 0.1);
			int attackValue = (int)(diffVal * 0.2);
			Enemy e {randomY, randomX, life, motionRange, attackRange, attackValue};
			enemies.push_back(e);
		}
	}
}

// All enemies in range receive damage
// Remove dead enemies from vector and map
int Room::attackEnemies(int y, int x, int playerRange, int value) {
	int dScore = 0;
	for (int i = 0; i < enemies.size(); i++) {
		if (enemies[i].getY() == y) {
			if (abs(enemies[i].getX() - x) <= playerRange) {
				enemies[i].decrLife(value);
				enemies[i].setHit(true);
				if (enemies[i].getLife() <= 0) {
					dScore += enemies[i].getAttackValue();
					enemies.erase(enemies.begin() + i);
				}
			}
		}
	}
	return dScore;
}

// All enemies in range attack the Player
int Room::attackPlayer(int y, int x) {
	int damage = 0;
	for (int i = 0; i < enemies.size(); ++i) {
		if (enemies[i].getY() == y) {
			if (abs(enemies[i].getX() - x) <= enemies[i].getAttackRange()) {
				damage += enemies[i].getAttackValue();
			}
		}
	}
	return damage;
}

Bonus* Room::getBonus(int y, int x) const {
	for (int i = 0; i < bonuses.size(); ++i) {
		if (bonuses[i].getY() == y && bonuses[i].getX() == x) {
			return const_cast<Bonus*>(&bonuses[i]);
		}
	}
	return nullptr;
}

Enemy* Room::getEnemy(int y, int x) const {
	for (int i = 0; i < enemies.size(); ++i) {
		if (enemies[i].getY() == y && enemies[i].getX() == x) {
			return const_cast<Enemy*>(&enemies[i]);
		}
	}
	return nullptr;
}

int Room::getEntranceY() const {
	return entrance;
}

int Room::getExitY() const {
	return exit;
}

int Room::getHeight() const {
	return h;
}

int Room::getWidth() const {
	return w;
}

Tile Room::operator()(int y, int x) const {
	assert(x >= 0 && x < w && y >= 0 && y < h);
	if (getEnemy(y, x) != nullptr) {
		return Tile::Enemy;
	} else if (getBonus(y, x) != nullptr) {
		return Tile::Bonus;
	}
	return room[y * w + x];
}

void Room::removeBonus(int y, int x) {
	for (int i = 0; i < bonuses.size(); ++i) {
		if (bonuses[i].getY() == y && bonuses[i].getX() == x) {
			bonuses.erase(bonuses.begin() + i);
			return;
		}
	}
}

// Randomly move all enemies
void Room::update() {
	for (int i = 0; i < enemies.size(); ++i) {
		int dir = randomInt(-1, 1);
		if (dir == 0) {
			continue;
		}
		if (abs(enemies[i].getX() + dir - enemies[i].getFixedX()) <= enemies[i].getMotionRange()
			&& ((*this)(enemies[i].getY() + 1, enemies[i].getX() + dir) == Tile::Wall
				|| (*this)(enemies[i].getY() + 1, enemies[i].getX() + dir) == Tile::Ladder)
			&& ((*this)(enemies[i].getY(), enemies[i].getX() + dir) == Tile::Empty
				|| (*this)(enemies[i].getY(), enemies[i].getX() + dir) == Tile::Bonus)) {
			enemies[i].move(dir);
		}
	}
}

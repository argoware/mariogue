// SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include "Enemy.h"
#include "Room.h"
#include "Tile.h"

#include <ncurses.h>

class Player {
protected:
	Vector<Room> rooms;
	long seed;
	int roomIndex;
	int lifePoints; // Initialised to 100
	bool hit;
	int yLoc; // Current y coordinate
	int xLoc; // Current x coordinate
	char character; // Player appearance, '@' as default
	WINDOW* win;
	int score; // Initialised to 0
	int attackRange;
	int attackValue;
	void bleed();

public:
	Player(WINDOW* win, long seed);
	void attack();
	void changeAttackRange(int n);
	void changeAttackValue(int n);
	void changeLife(int n);
	Room* curRoom();
	void display(int xmax);
	int getIndex() const;
	int getMv();
	int getScore() const;
	void incrScore(int n);
	bool isDead();
	void move(int deltaX, int deltaY);
	void nextRoom();
	void prevRoom();
	void printRoom();
	void updatePlayer();
};

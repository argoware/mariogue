// SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

class Enemy {
protected:
	int y; // Current y coordinate
	int x; // Current x coordinate
	int fixed_x; // Fixed location, centre of movement
	int lifePoints;
	bool hit;
	int motionRange; // Range of horizontal motion (on each side)
	int attackRange;
	int attackValue;

public:
	Enemy();
	Enemy(int y, int x, int life, int motionRange, int attackRange, int attackValue);
	void decrLife(int n);
	int getAttackRange() const;
	int getAttackValue() const;
	int getFixedX() const;
	bool getHit() const;
	int getLife() const;
	int getMotionRange() const;
	int getX() const;
	int getY() const;
	void move(int deltaX);
	void setHit(bool a);
};

// SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

template <typename T>
class Vector {
private:
	T value;
	Vector* next;
	int s;

public:
	Vector();
	Vector(T v);
	Vector(int n, T value);
	int begin() const;
	void erase(int i);
	T& operator[](int i);
	T& operator[](int i) const;
	void push_back(T value);
	void print();
	int size() const;
};

template <typename T>
Vector<T>::Vector() {
	next = nullptr;
	s = 0;
}

template <typename T>
Vector<T>::Vector(T v) {
	value = v;
	next = nullptr;
	s = 1;
}

template <typename T>
Vector<T>::Vector(int n, T value) {
	next = nullptr;
	s = 0;
	for (int i = 0; i < n; ++i) {
		push_back(value);
	}
}

template <typename T>
int Vector<T>::begin() const {
	return 0;
}

template <typename T>
void Vector<T>::erase(int i) {
	if (s > 1) {
		if (i > 0) {
			if (next->next != nullptr) {
				next->erase(--i);
			} else {
				delete next;
				next = nullptr;
			}
		} else {
			Vector<T>* tmp = next;
			value = next->value;
			next = next->next;
			delete tmp;
		}
	}
	--s;
}

template <typename T>
T& Vector<T>::operator[](int i) {
	if (i == 0) {
		return value;
	} else {
		return (*next)[--i];
	}
}

template <typename T>
T& Vector<T>::operator[](int i) const {
	if (i == 0) {
		return const_cast<T&>(value);
	} else {
		return (*next)[--i];
	}
}

template <typename T>
void Vector<T>::push_back(T value) {
	if (s == 0) {
		this->value = value;
	} else if (next == nullptr) {
		Vector<T>* v = new Vector<T>(value);
		this->next = v;
	} else {
		next->push_back(value);
	}
	++s;
}

template <typename T>
int Vector<T>::size() const {
	return s;
}

// SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include "Bonus.h"
#include "Enemy.h"
#include "Tile.h"
#include "Vector.h"

#include <cassert>
#include <random>

class Room {
private:
	int w;
	int h;
	int entrance;
	int exit;
	Vector<Tile> room;
	Vector<Enemy> enemies;
	Vector<Bonus> bonuses;
	std::mt19937 gen;
	int randomInt(int min, int max);

public:
	void set(int y, int x, Tile tile);
	Room();
	Room(long int seed, int height = 0, int width = 0, int diff = 1);
	int attackEnemies(int y, int x, int range, int value);
	int attackPlayer(int y, int x);
	Bonus* getBonus(int y, int x) const;
	Enemy* getEnemy(int y, int x) const;
	int getEntranceY() const;
	int getExitY() const;
	int getHeight() const;
	int getWidth() const;
	Tile operator()(int y, int x) const;
	void removeBonus(int y, int x);
	void update();
};

// SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include "Effect.h"

#include <cassert>

class Bonus {
private:
	int x;
	int y;
	Effect effect;
	int value;

public:
	Bonus();
	Bonus(int y, int x, Effect effect, int value);
	Effect getEffect() const;
	int getValue() const;
	int getX() const;
	int getY() const;
};

// SPDX-FileCopyrightText: 2021 Luca Bassi, Mirco Dondi
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

enum class Tile {
	Empty,
	Wall,
	Door,
	Ladder,
	Bonus,
	Enemy
};
